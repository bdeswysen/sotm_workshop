# <span style="color:darkblue">Introduction<span>
    


Welcome to the "How to put an entire country in a container: transportable OpenStreetMap data integration into Postgis using docker" hands-on session!


We will explore the seamless integration of OpenStreetMap (OSM) data in PostGIS using the Imposm tool within a Dockerized environment. The workshop is structured to guide you through the entire process:

1. Peparing your setup to learn Docker basics, 
2. Setting up a PostGIS database, 
3. Understanding Imposm's functionalities including 
    - the mapping between OSM keywords and Postgresql table attributes 
    - the command to load medium to big OSM datasets into Postgis
    - how to build a docker stack combining imposm and postgis
4. Interacting with the loaded OSM data with QGIS and SQL,
5. Enhancing your Docker stack for improved performance. 

Whether you're a GIS enthusiast, developer, or data professional, this workshop provides a practical and comprehensive approach to harnessing the synergy between OSM, PostGIS, and Docker. 


